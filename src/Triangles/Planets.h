#pragma once
#include <glm/glm.hpp>
#include "Sphere.h"
#include "shader.h"

using glm::vec4;
using glm::vec3;
using glm::vec2;
using glm::mat4x4;
using glm::mat3x3;

const unsigned SPHERE_QUALITY = 30;
extern double SCALING_COEF;
extern double RADIUS_COEF;
extern double SKYMAP_COEF;
const float km_to_meters = 1e+3;

extern int last_frame_time;

struct Material
{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	vec4 emission;
	float shininess;
};

class Planet {
	Sphere data;
	float radius;
	float angle; // between y and rotation axis
	float rotation_time; 
	float rotation_angle;
	vec3 rotation_axis;
	vec3 offset;
	char *texture_file;
	void *texture;  // well. This is EasyBMP_Texture *, but that lib is so
					// stupid I had to use void* to have it compiled
	unsigned texture_id;
	struct Material material;
	vec3 temp_coordinates;
	vec3 temp_speed;
	vec3 coordinates;
	vec3 speed;
	float mass;
	unsigned id; // this is used as planed id
	bool is_planet; // if it's not a planet, gen_offsets() returns 0;
	unsigned program_id;
	vec3 gravitation_function();
public:
	Planet(float r, char *texture_img, unsigned i, unsigned program_id);
	Planet(void);
	~Planet(void) {delete texture;}
	void init_texture(void);
	void init_data(unsigned program_id);
	void draw(mat4x4 viewMatrix, int m_location, int n_location, int tex_location);
	void set_r(float r) {radius = r;}
	void set_tex_file(char *src) {texture_file = src;}
	void set_tex_id(unsigned i) {texture_id = i;}
	void set_id(unsigned i) {id = i;}
	void set_mass(float m) {mass = m;}
	void set_position(vec3 src) {coordinates = vec3(src.x, src.z, src.y) * km_to_meters;}
	void set_speed(vec3 src) {speed = vec3(src.x, src.z, src.y) * km_to_meters;}
	void set_planet(bool param) {is_planet = param;}
	void set_axis(vec3 src) {rotation_axis = src;}
	void set_rotation_time(float src) {rotation_time = src;}
	void set_material(struct Material src) {material = src;}
	float get_radius() {return radius;}
	void set_coordinates(vec3 src) {coordinates = src;}
	vec3 get_coordinates() {return coordinates;}
	void gen_rotation(float delta);
	void gen_offsets();
	void switch_temp();
};

struct PlanetInfo {
	float r;
	struct Material material;
	char *texture_img;
	vec3 position;
	vec3 speed;
	float mass;
	float axis;
	float T;
}; // supporting struct to fill Planets array

struct LightSource
{
	vec4 position;
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
}; // struct for light source

void light_source_setup(unsigned program_id, struct LightSource &light);
void material_setup(unsigned program_id, struct Material &material);
template<typename T> int sgn(T val);
vec3 get_shifted_coordinates(vec3 coordinates);
vec3 get_cartesian_from_spherical_camera_mod(vec3 &src);
vec3 get_cartesian_from_spherical(vec3 src);
vec3 get_spherical_from_cartesian(vec3 src);

extern Planet planets[];
extern const unsigned N_PLANETS;
extern float dt;
extern float time_multiplier; // world's seconds in one read second
extern struct LightSource sun_light;
extern bool drawing_mode;
// extern unsigned time;