// standard libraries
#define _USE_MATH_DEFINES
#include <cmath>
#include <iostream>
using namespace std;

// opengl headers
#include <GL/glew.h>
#include <GL/freeglut.h>

// opengl mathematics
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/matrix_inverse.hpp>

// BMP library
// #include "EasyBMP.h"
// #include "EasyBMP_OpenGL.h"

// functions for shader compilation and linking
#include "shaderhelper.h"

// object for drawing
#include "Sphere.h"
#include "Planets.h"

using glm::mat4x4;

const unsigned N_PLANETS = 10;

struct Material A = {
	vec4(0.0f, 0.0f, 0.0f, 0.0f),
	vec4(1.0f, 1.0f, 1.0f, 1.0f),
	vec4(1.0f, 1.0f, 1.0f, 1.0f),
	vec4(0, 0, 0, 0),
	20.0f
};

struct Material B = {
	vec4(0.1f, 0.1f, 0.1f, 0.1f),
	vec4(0.6f, 0.6f, 0.6f, 0.6f),
	vec4(0.8f, 0.8f, 0.8f, 0.8f),
	vec4(0, 0, 0, 0),
	10.0f
};

struct Material C = {
	vec4(0.0f, 0.0f, 0.0f, 0.0f),
	vec4(0.0f, 0.0f, 0.0f, 0.0f),
	vec4(0.0f, 0.0f, 0.0f, 0.0f),
	vec4(0.9f, 0.9f, 0.9f, 0.9f),
	0.0f
};

struct Material sky_material = {
	vec4(0.0f, 0.0f, 0.0f, 0.0f),
	vec4(0.0f, 0.0f, 0.0f, 0.0f),
	vec4(0.0f, 0.0f, 0.0f, 0.0f),
	vec4(0.7f, 0.7f, 0.7f, 0.7f),
	0.0f
};

struct PlanetInfo planet_data[] = {
	{69599.0f, C, "./textures/sunmap.bmp", vec3(9.166368072918009E+04, -3.587908596635106E+05, -1.263365847270218E+04), vec3(1.068407615399651E-02,  2.825998901862247E-03, -2.451813399270692E-04), 1.99E+30, 7.25f, 25.38f},
	{2439.7f, A, "./textures/mercury.bmp", vec3(3.981532574646779E+07,  2.693705871595619E+07, -1.427017526993466E+06), vec3(-3.712878513620788E+01,  4.226944115449314E+01,  6.860766793768278E+00), 3.33E+23, 2.11f, 58.646f},
	{6051.8f, B, "./textures/venus.bmp", vec3(1.083995110582697E+08, -7.516123504840386E+06, -6.361445678009944E+06), vec3(2.163430099495670E+00,  3.478693145745870E+01,  3.521473884663605E-01), 4.87E+24, 177.36f, 243.023f},
	{6371.0f, A, "./textures/earth.bmp", vec3(1.162398804913440E+08,  9.214935380493926E+07, -1.560961036842257E+04), vec3(-1.903255921308336E+01,  2.319272201812972E+01, -9.394261929108316E-04), 5.97E+24, 23.44f, 0.99726968f},
	{3389.5f, A, "./textures/mars.bmp", vec3(-1.528793878916806E+08,  1.925541334868633E+08,  7.784109759604024E+06), vec3(-1.805947412894641E+01, -1.298811643007845E+01,  1.711362049767525E-01), 6.42E+23, 25.1919f, 1.029f},
	{69911.0f, B, "./textures/jupiter.bmp", vec3(-1.308404872563110E+08,  7.618480179221750E+08, -2.483656477704518E+05), vec3(-1.303624071468951E+01, -1.591321557884244E+00,  2.983270239243695E-01), 1.90E+27, 3.13f, 0.4147f},
	{57316.0f, B, "./textures/saturn.bmp", vec3(-1.062988073620396E+09, -1.022591702447227E+09,  6.008502801056857E+07), vec3(6.170956639472989E+00, -6.985778300302598E+00, -1.241099702155237E-01), 5.68E+26, 26.73f, 0.4416f},
	{25266.0f, B, "./textures/uranus.bmp", vec3(2.946083841697208E+09,  5.529078700946414E+08, -3.611419689746372E+07), vec3(-1.305921743797635E+00,  6.375718119862134E+00,  4.060658963891937E-02), 8.68E+25, 97.77f, 0.71833f},
	{24552.5f, A, "./textures/neptune.bmp", vec3(4.036568780165731E+09, -1.954926579794473E+09, -5.276881058283679E+07), vec3(2.332992134044233E+00,  4.924012112924557E+00, -1.551671101516667E-01), 1.02E+26, 28.32f, 0.6653f},
	{1195.0f, A, "./textures/pluto.bmp", vec3(9.074663629135412E+08, -4.775885697847751E+09,  2.485565548180889E+08), vec3(5.444809029005323E+00, -8.040967446408351E-02, -1.566356777898360E+00), 1.30E+22, 119.591f, 6.38723f},
	// {2595.0f, A, "./textures/moon.bmp", vec3(1.158649106064001E+08,  9.210860720133862E+07, -3.377896741176977E+04), vec3(-1.886643942685332E+01,  2.219137827829259E+01,  8.193245430657427E-02), 5.97E+24/81.3f, 6.67f, 3231.50f}
};

struct PlanetInfo skymap_data = {1.0e+4, sky_material, "./textures/skymap.bmp", vec3(0,  0, 0), vec3(0, 0, 0), 0, 0, 0};

Planet planets[N_PLANETS];
Planet skymap; // well, this isn't a planet, but this approach is easier

// struct for loading shaders
ShaderProgram shaderProgram;

//window size
int windowWidth = 800;
int windowHeight = 600;

//last mouse coordinates
int last_mouse_x, last_mouse_y;
const float MOUSE_SPEED = 1;
int camera_fixed_on = 0;
int ITERATIONS_PER_FRAME = 30;

bool mouse_button_on = true;
unsigned time = 0; // the time last display() was called

double SCALING_COEF = 5e+6;
double RADIUS_COEF = 5e+4;
double SKYMAP_COEF = 1;
bool drawing_mode = false; // true if real mode is used, false otherwise

//camera position
vec3 eye(0, 0, -150);
vec3 eye_spherical(500, 6.3727, 0.63); // r, phi, xi
//reference point position
vec3 cen(0.0, 0, 0);
//up vector direction (head of observer)
vec3 up(0, 1, 0);

struct LightSource sun_light = {
	vec4(0, 0, 0, 0),
	vec4(0.0f, 0.0f, 0.0f, 0.0f),
	vec4(0.8f, 0.8f, 0.8f, 0.8f),
	vec4(1.0f, 1.0f, 1.0f, 1.0f)
};

//matrices
mat4x4 modelViewMatrix;
mat4x4 projectionMatrix;
mat4x4 modelViewProjectionMatrix;
mat4x4 normalMatrix;

///defines drawing mode
bool useTexture = true;

char VertexShaderName[] = "Vertex.vert";
char FragmentShaderName[] = "Fragment.frag";

void switch_drawing_mode(bool real);

////////////////////////////////////////////////////////
///
void initTexture()
{
	for (unsigned i = 0; i < N_PLANETS; i++) {
		planets[i].init_texture();
	}
	skymap.init_texture();
}

/////////////////////////////////////////////////////////////////////
///is called when program starts
void init()
{
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_LINE_SMOOTH);
	glHint(GL_LINE_SMOOTH_HINT,  GL_NICEST);
	shaderProgram.init(VertexShaderName,FragmentShaderName);
	glUseProgram(shaderProgram.programObject);

	eye = get_cartesian_from_spherical(eye_spherical);
	switch_drawing_mode(drawing_mode);

	for (unsigned i = 0; i < N_PLANETS; i++) {
		planets[i].set_r(planet_data[i].r);
		planets[i].set_tex_file(planet_data[i].texture_img);
		planets[i].set_position(planet_data[i].position);
		planets[i].set_mass(planet_data[i].mass);
		planets[i].set_speed(planet_data[i].speed);
		planets[i].set_axis(vec3(0.0f, planet_data[i].axis, 0.0f));
		planets[i].set_rotation_time(planet_data[i].T);
		planets[i].set_material(planet_data[i].material);
		planets[i].set_id(i);
		planets[i].init_data(shaderProgram.programObject);
	}

	skymap.set_r(skymap_data.r);
	skymap.set_tex_file(skymap_data.texture_img);
	skymap.init_data(shaderProgram.programObject);
	skymap.set_planet(false);
	skymap.set_material(skymap_data.material);

	initTexture();
}


/////////////////////////////////////////////////////////////////////
///called when window size is changed
void reshape(int width, int height)
{
	windowWidth = width;
	windowHeight = height;
	//set viewport to match window size
	glViewport(0, 0, width, height);

	float fieldOfView = 45.0f;
	float aspectRatio = float(width)/float(height);
	float zNear = 0.01f;
	float zFar = 11000.0f;
	//set projection matrix
	projectionMatrix = glm::perspective(fieldOfView, aspectRatio, zNear, zFar);
}

////////////////////////////////////////////////////////////////////
///actions for single frame
void display()
{
	glClearColor(0, 0, 0, 0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

	glUseProgram(shaderProgram.programObject);

	cen = get_shifted_coordinates(planets[camera_fixed_on].get_coordinates());

	eye = get_cartesian_from_spherical_camera_mod(eye_spherical);

	//camera matrix. camera is placed in point "eye" and looks at point "cen".
	mat4x4 viewMatrix = glm::lookAt(eye, cen, up);
	
	int vp_location = glGetUniformLocation(shaderProgram.programObject, "viewProjectionMatrix");
	int m_location = glGetUniformLocation(shaderProgram.programObject, "modelMatrix");
	int n_location = glGetUniformLocation(shaderProgram.programObject, "normalMatrix");
	int tex_location = glGetUniformLocation(shaderProgram.programObject, "texture");
	int cam_loc = glGetUniformLocation(shaderProgram.programObject, "camera_position");

	if (vp_location < 0 || m_location < 0 || n_location < 0 || tex_location < 0 || cam_loc < 0) {
		glClearColor(0, 0, 1, 1);
		glClear(GL_COLOR_BUFFER_BIT);
	} else {
		glUniformMatrix4fv(vp_location, 1, 0, glm::value_ptr(projectionMatrix*viewMatrix));
		int cur_frame_time = glutGet(GLUT_ELAPSED_TIME);
		float delta = time_multiplier*(cur_frame_time - last_frame_time)/1e+3;
		last_frame_time = cur_frame_time;
		dt = delta/ITERATIONS_PER_FRAME;
		// clog << dt << ' ';
		if (time_multiplier > 0.5)
			for (unsigned t = 0; t < delta; t += dt) {
				for (unsigned i = 0; i < N_PLANETS; i++) {
					planets[i].gen_offsets();
					planets[i].gen_rotation(dt);
				}
				for (unsigned i = 0; i < N_PLANETS; i++)
					planets[i].switch_temp();
			}
		glUniform3fv(cam_loc, 1, glm::value_ptr(eye));
		for (unsigned i = 0; i < N_PLANETS; i++) {
			planets[i].draw(viewMatrix, m_location, n_location, tex_location);
		}
		skymap.set_coordinates(eye);
		skymap.draw(viewMatrix, m_location, n_location, tex_location);	
	}
	
	glutSwapBuffers();
}

//////////////////////////////////////////////////////////////////////////
///IdleFunction
void update()
{
	//make animation
	glutPostRedisplay();
}

vec3 get_cartesian_from_spherical_camera_mod(vec3 &src) {
	float r = src.x, phi = src.y, xi = src.z;
	if (r < 0.001f)
		r = 0.001f;
	if (xi >= M_PI/2)
		xi = (float)0.9999*M_PI/2;
	else if (xi <= -M_PI/2)
		xi = (float)-0.9999*M_PI/2;
	src = vec3(r, phi, xi);
	xi -= (float)M_PI/2;
	return vec3(r*cosf(phi)*sinf(xi), r*cosf(xi), r*sinf(phi)*sinf(xi)) + cen;
}

vec3 get_cartesian_from_spherical(vec3 src) {
	float r = src.x, phi = src.y, xi = src.z;
	if (r < 0.001f)
		r = 0;
	src = vec3(r, phi, xi);
	return vec3(r*cosf(phi)*sinf(xi), r*sinf(phi)*sinf(xi), r*cosf(xi));
}

vec3 get_spherical_from_cartesian(vec3 src) {
	double r = sqrt(pow(src.x, 2.0) + pow(src.y, 2.0) + pow(src.z, 2.0));
	if (r < 0.001f)
		return vec3(0);
	else
		return vec3(r, atan2(src.y, src.x), acos(src.z/r));
}

void switch_drawing_mode(bool real) {
	if (real) {
		SCALING_COEF = 7e+7;
		RADIUS_COEF = 7e+4; 
		SKYMAP_COEF = 1;
	} else {
		SCALING_COEF = 5e+7;
		RADIUS_COEF = 6e+2;
		SKYMAP_COEF = 1;
	}
}

/////////////////////////////////////////////////////////////////////////
///is called when key on keyboard is pressed
///use SPACE to switch mode
///TODO: place camera transitions in this function
void keyboard(unsigned char key, int mx, int my)
{
	switch (key) {
		case 'd':
			eye_spherical += vec3(0, 0.05, 0);
			break;
		case 'a':
			eye_spherical += vec3(0, -0.05, 0);
			break;
		case 's':
			eye_spherical += vec3(0, 0, -0.05);
			break;
		case 'w':
			eye_spherical += vec3(0, 0, 0.05);
			break;
		case '=':
			if (drawing_mode)
				eye_spherical.x /= 1.1f;
			else 
				eye_spherical.x -= 5;
			break;
		case '-':
			if (drawing_mode)
				eye_spherical.x *= 1.1f;
			else 
				eye_spherical.x += 5;
			// if (eye_spherical.x >= skymap.get_radius()/SKYMAP_COEF)
				// eye_spherical.x = (float)0.99*skymap.get_radius()/SKYMAP_COEF;
			break;
		case '[':
			time_multiplier /= 1.3;
			ITERATIONS_PER_FRAME = 30 + time_multiplier/1e+5;
			if (time_multiplier < 36000) {
				time_multiplier = 0;
			}
			clog << "time coefficient: " << time_multiplier << endl;
			break;
		case ']':
			time_multiplier *= 1.3;
			if (time_multiplier < 1) {
				// cout << "time started" << endl;
				// time_multiplier = 36000;
				time_multiplier = 36000;
			} else if (time_multiplier > 5e+06) {
				time_multiplier = 5e+06;
			}
			ITERATIONS_PER_FRAME = 30 + time_multiplier/1e+5;
			clog << "time coefficient: " << time_multiplier << endl;
			break;
		case 'r':
			drawing_mode = !drawing_mode;
			switch_drawing_mode(drawing_mode);
			break;
		case ' ':
			if (time_multiplier < 5) {
				time_multiplier = 36000;
			} else {
				time_multiplier = 0;
			}
			clog << "time coefficient: " << time_multiplier << endl;
			break;
		default:
			if (key >= '0' && key <= '9') {
				cen = get_shifted_coordinates(planets[key - '0'].get_coordinates());
				camera_fixed_on = key - '0';
			}
	}
	eye = get_cartesian_from_spherical(eye_spherical);
}

/////////////////////////////////////////////////////////////////////////
///is called when mouse button is pressed
///TODO: place camera rotations in this function
void mouse(int button, int mode, int posx, int posy)
{
	if (button == GLUT_LEFT_BUTTON || button == GLUT_RIGHT_BUTTON) {
		if (mode == GLUT_DOWN) {
			last_mouse_x = posx; 
			last_mouse_y = posy;
			mouse_button_on = true;
		} else if (mode == GLUT_UP) {
			mouse_button_on = false;
		}
	}
	
}

void mouse_motion(int x, int y)
{
	if (!mouse_button_on) {
		cout << "!mouse_button_on in mouse_motion()" << endl;
		return;
	}
	double radius = (eye_spherical.x > 100 ? eye_spherical.x : 100);
	eye_spherical.y += MOUSE_SPEED*atan((x - last_mouse_x)/radius);
	eye_spherical.z += MOUSE_SPEED*atan((y - last_mouse_y)/radius);
	eye = get_cartesian_from_spherical_camera_mod(eye_spherical);
	last_mouse_x = x;
	last_mouse_y = y;
}

////////////////////////////////////////////////////////////////////////
///this function is used in case of InitializationError
void emptydisplay()
{
	;
}

////////////////////////////////////////////////////////////////////////
///entry point
int main (int argc, char* argv[])
{
	glutInit(&argc, argv);
#ifdef __APPLE__
	glutInitDisplayMode( GLUT_3_2_CORE_PROFILE| GLUT_RGBA | GLUT_DOUBLE );
#else
	glutInitDisplayMode( GLUT_RGBA | GLUT_DOUBLE );
	glutInitContextVersion (3, 2);  
	glutInitContextFlags (GLUT_FORWARD_COMPATIBLE);
	glewExperimental = GL_TRUE;
#endif
	glutCreateWindow("Test OpenGL application");
	glutDisplayFunc(display);
	glutReshapeFunc(reshape);
	glutReshapeWindow(windowWidth,windowHeight);
	glutIdleFunc(update);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutMotionFunc(mouse_motion);

	glewInit();
	const char * slVer = (const char *) glGetString ( GL_SHADING_LANGUAGE_VERSION );
	cout << "GLSL Version: " << slVer << endl;

	try {
		init();
	}
	catch (const char *str) {
		cout << "Error During Initialiation: " << str << endl;
		glutDisplayFunc(emptydisplay);
		glutMainLoop();
		return -1;
	}


	try {
		glutMainLoop();
	}
	catch (const char *str) {
		cout << "Error During Main Loop: " << str << endl;
	}
	return 0;
}
